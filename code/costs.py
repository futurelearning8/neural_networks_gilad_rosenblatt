import numpy as np
from abc import ABC, abstractmethod


class Loss(ABC):
    """Interface for a loss function."""

    @classmethod
    def calculate(cls, predictions, ground_truth, average=True):
        """
        Calculate the loss for the input predictions compared to the ground-truth.

        :param np.ndarray predictions: predictions array of dimensions (number_of_samples, ?).
        :param np.ndarray ground_truth: ground truth array of dimensions (number_of_samples, ?).
        :param bool average: if True return average loss per sample instead of the total loss.
        :return np.ndarray: a scalar non-negative float representing the overall loss penalty.
        """
        total_loss = cls._total_loss(predictions=predictions, ground_truth=ground_truth)
        if average:
            return total_loss / ground_truth.shape[0]
        else:
            return total_loss

    @staticmethod
    @abstractmethod
    def _total_loss(predictions, ground_truth):
        """Calculate the total loss (not the loss per sample)."""
        pass


class MeanSquareError(Loss):
    """Mean square error loss."""

    @staticmethod
    def _total_loss(predictions, ground_truth):
        """
        Evaluate the sum of squared errors for the input predicted values and ground-truth values (average=True makes
        it into mean squared error in loss method).

        :param np.ndarray predictions: predictions array of dimensions (number_of_samples, number_of_targets).
        :param np.ndarray ground_truth: ground truth values array of dimensions (number_of_samples, number_of_targets).
        :return np.ndarray: a scalar non-negative float representing the overall loss across all samples.
        """
        return np.sum(np.square(predictions - ground_truth)) / 2  # Sum along rows (targets) and columns (samples).


class CrossEntropy(Loss):
    """Cross entropy loss."""

    @staticmethod
    def _total_loss(predictions, ground_truth):
        """
        Evaluate the total cross entropy loss for the input predicted probabilities and ground-truth labels.

        :param np.ndarray predictions: probabilities array of dimensions (number_of_samples, number_of_classes).
        :param np.ndarray ground_truth: one-hot-encoded labels of dimensions (number_of_samples, number_of_classes).
        :return np.ndarray: a scalar non-negative float representing the overall loss across all samples.
        """
        return -np.sum(ground_truth * np.log(predictions))  # Sum along rows (classes) and columns (samples).


class BinaryCrossEntropy(Loss):
    """Binary cross entropy loss."""

    @staticmethod
    def _total_loss(predictions, ground_truth):
        """
        Evaluate the total binary cross entropy loss for the input predicted probabilities and ground-truth labels.

        :param np.ndarray predictions: probabilities array of dimensions (number_of_samples, number_of_classes=1).
        :param np.ndarray ground_truth: 0-1 labels array of dimensions (number_of_samples, number_of_classes=1).
        :return np.ndarray: a scalar non-negative float representing the overall loss across all samples.
        """
        return -np.sum(ground_truth * np.log(predictions) + (1 - ground_truth) * np.log(1 - predictions))
