import numpy as np
from abc import ABC, abstractmethod
from tensors import Weights, Bias


class Layer(ABC):
    """Interface for a layer in a computational graph."""

    def __init__(self, trainable=False, name=None):
        """
        Initialize layer attributes and set trainable flag (are there weights to learn in this layer?).

        :param bool trainable: True if there are learnable layer weights.
        :param str name: name of the layer instance.
        """

        # Instantiate last input, output and gradient variables.
        self._inputs = None
        self._outputs = None
        self._gradient = None

        # Save trainable flag (for use by an optimizer).
        self._trainable = trainable

        # Save layer name (for use by __str__ in visual debugger).
        self.name = name

    def __str__(self):
        """
        :return str: this instance name string (helps to identify it in visual debugger).
        """
        return f"Layer: {self.name}"

    @abstractmethod
    def forward(self, inputs):
        """Propagate inputs across the layer and return output."""
        pass

    @abstractmethod
    def backprop(self, gradient_upstream):
        """Backpropagate the upstream gradient through this layer and return the total gradient including this layer."""
        pass

    @property
    def inputs(self):
        """Return inputs last used to forward propagate this layer."""
        return self._inputs

    @property
    def outputs(self):
        """Return outputs for inputs last used to forward propagate this layer."""
        return self._outputs

    @property
    def trainable(self):
        """Return the value of the trainable flag (if there are weights to learn in this layer)."""
        return self._trainable


class Linear(Layer):
    """A linear computational layer with trainable weights and biases that keeps track of their gradients."""

    def __init__(self, num_inputs, num_outputs, *args, **kwargs):
        """
        Initialize a linear layer that connects the given number of input and output features.

        :param int num_inputs: number of input features.
        :param int num_outputs: number of output features.
        """
        super().__init__(trainable=True, *args, **kwargs)
        self.weights = Weights(num_inputs=num_inputs, num_outputs=num_outputs)
        self.bias = Bias(num_outputs=num_outputs)
        self._num_inputs = num_inputs
        self._num_outputs = num_outputs

    def forward(self, inputs):
        """
        Propagate inputs across the layer using its current weights and biases.

        :param np.ndarray inputs: layer inputs of dimensions (rows=number_of_samples, columns=number_of_input_features).
        :return np.ndarray: output array for this layer (rows=number_of_samples, columns=number_of_output_features).

        NOTE: if one defines X=inputs.T such that each column consists of all features of a single sample,
        and defines W = weights.tensor.T and b = bias.tensor.T, then it follows that Y = WX + b for Y=outputs.T.
        """
        self._inputs = inputs
        self._outputs = np.matmul(self._inputs, self.weights.tensor) + self.bias.tensor
        return self._outputs

    def backprop(self, gradient_upstream):
        """
        Back-propagate the upstream gradient through this layer using its current weights and biases and update
        the gradients both passing across this layer (self._gradiant) and of the weights and bias terms.

        :param np.ndarray gradient_upstream: upstream gradient sized (number_of_samples, number_of_output_features).
        :return np.ndarray: gradient for this layer sized (number_of_samples, number_of_input_features).
        """
        self._gradient = np.matmul(gradient_upstream, self.weights.tensor.T)  # dJ/dA_previous = dJ/dZ * dZ/dA_previous.
        self.weights.gradient = np.matmul(self._inputs.T, gradient_upstream)  # dJ/dW = dJ/dZ * dZ/dW
        self.bias.gradient = gradient_upstream.sum(axis=0, keepdims=True)  # dJ/db = dJ/dZ * dZ/db = dJ/dZ * [1 ... 1]
        return self._gradient

    @property
    def num_inputs(self):
        return self._num_inputs

    @property
    def num_outputs(self):
        return self._num_outputs
