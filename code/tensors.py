import numpy as np


class Tensor:
    """Tensor that connects layer inputs and outputs and assert data type and dimensions on assignment."""

    def __init__(self, num_inputs, num_outputs, random_values=True):
        """
        Initialize a tensor that connects a given number of inputs and outputs. The Tensor has
        dimensions of (rows=number_of_inputs, columns=number_of_outputs).

        :param int num_inputs: number of inputs (rows).
        :param int num_outputs: number of outputs (columns).
        """

        # Save number or rows and columns (mean to be immutable).
        self._shape = (num_inputs, num_outputs)

        # Initiate tensor elements randomly (Gaussian with mean 0 and standard deviation 1) or with 0s.
        if random_values:
            self._tensor = np.random.normal(size=self.shape)
        else:
            self._tensor = np.zeros(shape=self.shape, dtype=float)

    @property
    def shape(self):
        """Return tensor dimensions."""
        return self._shape

    @property
    def tensor(self):
        """Return tensor values."""
        return self._tensor

    @tensor.setter
    def tensor(self, tensor):
        """Assert tensor dimensions and data type before setting it.

        :param np.ndarray tensor: array of appropriate dimensions and type np.float.
        """
        assert isinstance(tensor, np.ndarray) and tensor.shape == self.shape and tensor.dtype == np.float
        self._tensor = tensor


class TensorWithGradient(Tensor):
    """Tensor with a second tensor of the same dimensions (gradient) for which dimensions are asserted at assignment."""

    def __init__(self, *args, **kwargs):
        """Initialize a tensor and its gradient (to 0s)."""
        super().__init__(*args, **kwargs)
        self._gradient = np.zeros_like(self.tensor)

    @property
    def gradient(self):
        """Return the gradient tensor."""
        return self._gradient

    @gradient.setter
    def gradient(self, gradient):
        """
        Assert gradient dimensions and data type before setting it.

        :param np.ndarray gradient: array of appropriate dimensions and type np.float.
        """
        assert isinstance(gradient, np.ndarray) and gradient.shape == self.shape
        self._gradient = gradient


class Weights(TensorWithGradient):
    """Weights tensor with gradients that connect inputs and outputs of a layer."""

    def __init__(self, *args, **kwargs):
        """Randomly initialize a weights tensor with their gradients initialized with 0s."""
        super().__init__(random_values=True, *args, **kwargs)


class Bias(TensorWithGradient):
    """Bias vector with gradients that connect to outputs of a layer."""

    def __init__(self, *args, **kwargs):
        """Initialize a bias vector and its gradients with 0s."""
        super().__init__(num_inputs=1, random_values=False, *args, **kwargs)
