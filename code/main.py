import numpy as np
import matplotlib.pyplot as plt
from layers import Linear
from activations import ReLU, Softmax
from networks import NetworkGraph, GDOptimizer
from vis_utils import make_classification_dataset, plot_decision_boundary


def main():

    # Get toy dataset.
    X, y = make_classification_dataset(display=False)
    y = y.reshape(-1, 1)
    Y = np.hstack((1 - y, y))  # One-hot encoding (for 2 classes).

    # Create a network.
    linear1 = Linear(num_inputs=2, num_outputs=20, name="linear 1")
    relu = ReLU(name="relu")
    linear2 = Linear(num_inputs=20, num_outputs=2, name="linear 2")
    softmax = Softmax(name="softmax", is_last_layer=True)

    # Train the network.
    report_cycle = 10
    number_of_epochs = 500
    network = NetworkGraph(linear1, relu, linear2, softmax)
    optimizer = GDOptimizer(learning_rate=1e-4)
    costs = optimizer.optimize(
        network=network,
        X=X,
        Y=Y,
        number_of_epochs=number_of_epochs,
        verbose=True,
        return_loss=True,
        report_cycle=report_cycle
    )

    # Plot learning curve.
    plt.plot(np.arange(1, number_of_epochs, report_cycle), costs)
    plt.xlabel("Epochs")
    plt.ylabel("Total cross entropy loss")
    plt.title("Learning curve")
    plt.show()

    # Define function to predict class labels.
    def predict(X):
        Y_proba = network.forward(inputs=X)  # Calculate class probabilities (columns) for each sample (row).
        y_preds = np.argmax(Y_proba, axis=1)  # Find index of max probability along each row (sample).
        return y_preds

    # Report accuracy to console.
    accuracy = np.mean(predict(X=X) == y.ravel())
    print(f"Accuracy on train set: {accuracy:.2f}")

    # Plot decision boundary.
    plot_decision_boundary(X, y, predict)


if __name__ == "__main__":
    main()
