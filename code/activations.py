import numpy as np
from abc import ABC, abstractmethod
from layers import Layer


class Activation(Layer, ABC):
    """Interface for an activation layer."""

    def __init__(self, is_last_layer=False, *args, **kwargs):
        """Set the trainable flag to false (no weights to train for this layer type) and save is_last_layer status."""
        super().__init__(trainable=False, *args, **kwargs)
        self.is_last_layer = is_last_layer

    def forward(self, inputs):
        """
        Propagate inputs through this layer by element-wise operation.

        :param np.ndarray inputs: input array of dimensions (number_of_samples, number_of_features).
        :return np.ndarray: ReLU-ed input array of same dimensions (number_of_samples, number_of_features).
        """
        self._inputs = inputs
        self._outputs = self.function(self._inputs)
        return self._outputs

    def backprop(self, gradient_upstream):
        """
        Back-propagate the upstream gradient element-wise through this layer. Treat special case where this is the
        last layer by skipping gradient calculation for this layer and passing the input gradient to the output.

        :param np.ndarray gradient_upstream: input array of dimensions (number_of_samples, number_of_features).
        :return np.ndarray: gradient including this layer of same dimensions (number_of_samples, number_of_features).
        """
        if self.is_last_layer:
            # By convention input already includes activation (1st error = dJ/dZ_last = dJ/dA_last * dA_last/dZ_last).
            return gradient_upstream
        self._gradient = gradient_upstream * self.derivative(self._outputs)
        return self._gradient

    @staticmethod
    @abstractmethod
    def function(inputs):
        """Calculate the activation function element-wise for an array of inputs."""
        pass

    @staticmethod
    @abstractmethod
    def derivative(outputs):
        """Calculate the derivative of the activation function element-wise for an array of function outputs."""
        pass


class ReLU(Activation):
    """Rectified linear unit (ReLU) activation layer. Should not serve as last layer before loss for classification."""

    @staticmethod
    def function(inputs):
        """Calculate the ReLU function for an array of inputs."""
        return np.maximum(0, inputs)

    @staticmethod
    def derivative(outputs):
        """Calculate ReLU function derivative for an array of ReLU function outputs."""
        return np.maximum(0, outputs > 0)


class Sigmoid(Activation):
    """Sigmoid activation layer. Serves as the last layer before binary cross entropy loss for binary classification."""

    @staticmethod
    def function(inputs):
        """Calculate the sigmoid function for an array of inputs."""
        return 1 / (1 + np.exp(-inputs))

    @staticmethod
    def derivative(outputs):
        """Calculate sigmoid function derivative for an array of ReLU function outputs."""
        return outputs * (1 - outputs)


class Softmax(Activation):
    """Softmax activation layer. Serves as the last layer before cross entropy loss for multi-class classification."""

    @staticmethod
    def function(inputs):
        """Calculate the softmax function for an array of inputs."""
        exponents = np.exp(inputs - np.max(inputs))  # Guard against overflow (softmax is shift invariant).
        return exponents / exponents.sum(axis=1, keepdims=True)  # Sum along rows & broadcast column over rows.

    @staticmethod
    def derivative(outputs):
        """This method should not be called (it is the last layer so it just passes the first errors)."""
        raise NotImplemented
