import numpy as np
from costs import Loss, CrossEntropy


# TODO add visualization for training test case.
# TODO net should have the interface of model with train and fit (and an optimizer attribute).
# TODO Optimizer interface.
# TODO add batch GD + momentum + regularization.
# TODO make net backprop input be ground truth labels?
# TODO Benchmark against TensorFlow (without GPU)? :)
# TODO Dropout Layer 75% last layer. Moonset, overfit, noise=0.1-0.2.
# TODO assert "known loss-activation combination gradients" in network (first error).
# TODO Add loss function CE some 1e-8 to avoid log(0)?
# TODO one hot encoding for y labels to softmax dims? YES!!!


class NetworkGraph:
    """Computational graph of cascaded computational layers."""

    def __init__(self, *layers):
        """Save all inputs in-order to an ordered array of computational layers (net executes layers in sequence)."""
        self.layers = layers

    def forward(self, inputs):
        """
        Propagate inputs through the network (and save intermediate inputs and outputs at each layer).

        :param np.ndarray inputs: network inputs of dimensions (rows=number_of_samples, columns=number_of_features).
        :return np.ndarray: probability measures / target values sized (number_of_samples, number_of_classes/targets).
        """
        for layer in self.layers:
            inputs = layer.forward(inputs=inputs)
        return inputs

    def backprop(self, errors):
        """
        Back-propagate output errors and calculate all intermediate gradients downstream through the network.

        :param np.ndarray errors: network output errors to be back-propagated (Y_preds - Y), where Y is one-hot encoded.
        :return np.ndarray: derivative of loss over network inputs sized (number_of_samples, number_of_input_features).
        """
        for layer in reversed(self.layers):
            errors = layer.backprop(gradient_upstream=errors)
        return errors


class GDOptimizer:
    """Simple gradient descent optimizer."""

    def __init__(self, learning_rate=0.01, loss=CrossEntropy):
        """
        Initialize gradient descent optimizer with a given learning rate.

        :param float learning_rate: learning rate for gradient descent optimizer.
        :param loss: loss to optimize over given as a class object for a class that implements the Loss interface.
        """
        self.learning_rate = learning_rate
        self.loss = loss

    def optimize(self, network, X, Y, number_of_epochs, verbose=False, return_loss=False, report_cycle=100):
        """
        Run the optimizer on the neural network for a given train set and number of epochs.

        :param NetworkGraph network: neural network to train.
        :param np.ndarray X: features for train set samples of dimensions (number_of_samples, number_of_input_features).
        :param np.ndarray Y: probability measures / target values sized (number_of_samples, number_of_classes/targets).
        :param int number_of_epochs: number of epochs to train the network for.
        :param bool verbose: if True print loss calculated every report cycle.
        :param bool return_loss: if True return the loss values calculated every report cycle.
        :param int report_cycle: frequency in which to report loss given in number of epochs.
        """

        # Train network for given number of epochs.
        loss_values = []
        for epoch in range(number_of_epochs):

            # Forward and backward propagate the train set through the network.
            predictions = network.forward(inputs=X)
            network.backprop(errors=predictions - Y)  # First error term includes derivative of last activation.

            # Update the weights and bias terms of all trainable layers using their (internally calculated) gradients.
            for layer in network.layers:
                if layer.trainable:
                    layer.weights.tensor -= self.learning_rate * layer.weights.gradient
                    layer.bias.tensor -= self.learning_rate * layer.bias.gradient

            # Calculate loss every report cycle (if required) and print loss value (if required).
            if (return_loss or verbose) and epoch % report_cycle == 0:
                loss_value = self.loss.calculate(predictions=predictions, ground_truth=Y, average=False)
                if return_loss:
                    loss_values.append(loss_value)
                if verbose:
                    print('Loss function value: ', loss_value)

        # Return loss values (saved once every report cycle).
        if return_loss:
            return np.array(loss_values)
