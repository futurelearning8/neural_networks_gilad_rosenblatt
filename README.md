# Neural Networks

This is the introduction to neural network exercise for FL8 week 5 day 1 by Gilad Rosenblatt.

### Run

Run `main.py` from `code` as working directory.


### Results

The implemented neural network is demonstrated on sklearn's moons 2D 2-class dataset with 1024 samples and a noise parameter equal to 0.1. The example architecture is a fully connected layer from 2 features to 20 followed by ReLU activation, another fully connected layer from 20 to 2 features, and a softmax layer. A simple gradient descent optimizer minimized the cross entropy loss.

Below is a typical learning curve under the above scenario (varies according to random initialization).

![learning_curve](/images/learning_curve.png?raw=true).

The classifier achieves an (overfitted) accuracy score of 0.96 on the train set, with decision boundary plotted below.

![decision_boundary](/images/decision_boundary.png?raw=true).

## License

[WTFPL](http://www.wtfpl.net/)
